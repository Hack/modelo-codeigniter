<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Login extends CI_Controller {

	function __construct()
	{
    	parent::__construct();
  	}

    public function index()
    {
    	$data = array();

		if( $this->input->server('REQUEST_METHOD') == 'POST' ) {

			if($this->verificar_usuario() == true){
				redirect('dashboard', 'refresh');
			}else{
			  	$data["erro_validacao"] = 'Usuário ou senha inválido.';
			}
		} else {
			if($this->session->userdata('logged_in')){
				redirect('dashboard', 'refresh');
			}else{
				$this->session->sess_destroy();
			}
		}

    	$this->load->view('login/index',$data);
    }

  	public function verificar_usuario()
  	{
  		$this->load->model('Usuario_model','',true);
    	$usuario = $this->input->post('usuario');
    	$senha = $this->input->post('senha');
    
	    $result = $this->Usuario_model->Login($usuario, $senha);

    
	    if($result){
	      	foreach($result as $row){
	      		$meses = array(1 => 'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');

        		$this->session->set_userdata('logged_in', true);
				$this->session->set_userdata('id', $row->id);
				$this->session->set_userdata('perfil', $row->perfil);
				$this->session->set_userdata('id_perfil', $row->id_perfil);
				$this->session->set_userdata('usuario', $row->usuario);
				$this->session->set_userdata('nome', $row->nome);
				$this->session->set_userdata('img_perfil', $row->img_perfil);
				$this->session->set_userdata('data_cadastro', $meses[date("n",strtotime($row->data_cadastro))]." de ".date("Y",strtotime($row->data_cadastro)));

			}
		
			return true;
	    }else{
	      	return false;
	    }
  	}
	
	
    public function logout()
    {
    	$this->session->sess_destroy();
		redirect(base_url());
	}	
}
