<?php
Class Seguranca_model extends CI_Model
{
	private $menu = array();
	private $controller = null;
	private $action = null;

  	function __construct()
  	{
    	parent::__construct();
		$url = explode("/", $_SERVER['REQUEST_URI']);
		
		if(isset($url[1]) && !empty($url[1])){
			$this->controller = $url[1];
		}else{
			$this->controller = "login";
		}
		
		if(isset($url[2]) && !empty($url[2])){
			$this->action = $url[2];
		}else{
			$this->action = "index";
		}
		
		if(!$this->verifica_permissao()) {
			$this->session->set_userdata('erro_permissao', "Você não tem permissão para entrar na página de <b>".ucfirst($this->controller)."</b>!");
            redirect(base_url('dashboard'));
		}else{
			$this->session->set_userdata('erro_permissao', "");
		}

  	}

	public function verifica_permissao()
	{
		$action = array(
			"listar_clientes",
			"salvar_cliente",
			"alterar_cliente",
			"remover_cliente",
			"listar_produtos",
			"salvar_produto",
			"alterar_produto",
			"remover_produto"
		);
		
		if($this->controller != "login" && $this->controller != "dashboard" && $this->controller != "usuario" && !in_array($this->action, $action) ){

			if($this->session->userdata('logged_in') == true){

				$sql = $this->db->query("SELECT * FROM permissao_perfil	WHERE id_perfil = ?",array($this->session->userdata('id_perfil')));

				if($sql->num_rows() > 0){
					$permissoes = $sql->result_array();
					$status = false;
					
					foreach($permissoes as $dados){
						if($status === false){
							if($dados['controller'] == $this->controller && $dados['action'] == $this->action){
								$status = true;
							}
						}
					}

					if($status === true){
						return true;
					}else{
						return false;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return true;
		}
  	}

}
?>