<?php
Class Usuario_model extends CI_Model
{
	public function Login($usuario, $senha)
	{
		if(!empty($usuario) && !empty($senha)){
			$sql = $this->db->query("
				SELECT U.*,P.perfil,P.id_perfil FROM usuarios AS U
				INNER JOIN perfil AS P ON P.id_perfil = U.id_perfil
				WHERE U.usuario = ? 
				AND U.senha = ? LIMIT 1",array($usuario,MD5($senha)));
	
			if($sql->num_rows() == 1){
				return $sql->result();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}
?>